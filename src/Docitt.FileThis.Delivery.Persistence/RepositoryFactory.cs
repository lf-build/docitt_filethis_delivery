﻿using Docitt.FileThis.Delivery.Abstraction;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.FileThis.Delivery.Persistence
{
    public interface IRepositoryFactory
    {
        IFileThisDeliveryLogRepository CreateFileThisDeliveryRepository(ITokenReader reader, IConfiguration configuration);
    }

    public class RepositoryFactory : IRepositoryFactory
    {
        public RepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFileThisDeliveryLogRepository CreateFileThisDeliveryRepository(ITokenReader reader, IConfiguration configuration)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new FileThisDeliveryLogRepository(tenantService, mongoConfiguration);
        }
    }
}
