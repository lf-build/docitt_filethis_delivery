﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using Docitt.FileThis.Delivery.Abstraction;
using System.Threading.Tasks;

namespace Docitt.FileThis.Delivery.Persistence
{

    public class FileThisDeliveryLogRepository :
        MongoRepository<IDocumentMetaData, DocumentMetaData>, IFileThisDeliveryLogRepository
    {
        static FileThisDeliveryLogRepository()
        {
            BsonClassMap.RegisterClassMap<DocumentMetaData>(map =>
            {
                map.AutoMap();

                var type = typeof(DocumentMetaData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public FileThisDeliveryLogRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "filethisdeliverylog")
        {

        }

        public async Task<bool> AddDocumentMetaData(IDocumentMetaData metaData)
        {
            if(string.IsNullOrEmpty(metaData.TenantId))
                metaData.TenantId = TenantService.Current.Id;
            
            await Collection.InsertOneAsync(metaData);
            return true;
        }
    }
}