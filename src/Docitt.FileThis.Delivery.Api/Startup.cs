﻿using Docitt.FileThis.Delivery.Abstraction;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using Docitt.FileThis.Delivery;
using LendFoundry.DocumentManager.Client;
using Docitt.FileThis.Delivery.Persistence;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Http.Internal;
#endif
using LendFoundry.Foundation.ServiceDependencyResolver;

namespace Docitt.FileThis
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittFileThisDelivery"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[] { } }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.FileThis.Delivery.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
#else
               // services.AddSwaggerDocumentation();
#endif

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ConfigurationServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddTenantService();
            services.AddDocumentManager();
            services.AddEventHub(Settings.ServiceName);
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        
            services.AddMongoConfiguration(Settings.ServiceName);

            services.AddTransient<IConfiguration>(
                provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());

            services.AddTransient<IFileThisSignatureValidator, FileThisSignatureValidator>();

            services.AddTransient<IFileThisDeliveryListener, FileThisDeliveryListener>();

            services.AddTransient<IFileThisDeliveryServiceFactory, FileThisDeliveryServiceFactory>();

            services.AddTransient<IFileThisSignatureValidatorFactory, FileThisSignatureValidatorFactory>();

            services.AddTransient<LendFoundry.DocumentManager.IDocumentManagerService, DocumentManagerService>();

            services.AddTransient<IFileThisDeliveryLogRepository, FileThisDeliveryLogRepository>();

            services.AddTransient<IRepositoryFactory, RepositoryFactory>();

            // aspnet mvc related
            services
                .AddMvc()
                .AddLendFoundryJsonOptions();

            services.AddCors();

            // services.AddCaching();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);
                        // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT FileThis Delivery Service");
            });
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}