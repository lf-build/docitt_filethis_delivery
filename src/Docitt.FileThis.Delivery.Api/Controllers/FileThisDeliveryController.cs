﻿using Docitt.FileThis.Delivery.Abstraction;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
#else
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.FileThis.Api.Controllers
{
    /// <summary>
    /// Represents File this delivery controller class.
    /// </summary>
    public class FileThisDeliveryController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="fileThisDeliveryService"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="fileThisSignatureValidator"></param>
        /// <param name="logger"></param>
        public FileThisDeliveryController(
            IFileThisDeliveryListener fileThisDeliveryService, 
            IHttpContextAccessor httpContextAccessor,
            IFileThisSignatureValidator fileThisSignatureValidator,
            ILogger logger)
        {
            this.FileThisDeliveryListener = fileThisDeliveryService;
            this.HttpContextAccessor = httpContextAccessor;
            this.FilethisSignatureValidator = fileThisSignatureValidator;
            this.Logger = logger;
        }

        private IFileThisSignatureValidator FilethisSignatureValidator { get; }

        private IFileThisDeliveryListener FileThisDeliveryListener { get; }

        private IHttpContextAccessor HttpContextAccessor { get; }

        private new ILogger Logger { get; }

        /// <summary>
        /// Document
        /// </summary>
        /// <param name="tenantId">tenantName</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("filethis/{tenantId}/callback")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Document(string tenantId)
        {
            this.Logger.Debug("Inside Document... :");

            return await ExecuteAsync(async () =>
            {
                var context = HttpContextAccessor.HttpContext;
                try
                {
                    this.Logger.Debug("Authenticating sender...");
                    if (!await ValidateRequest(context))
                    {
                       this.Logger.Error("Request is not valid");
                       return ErrorResult.BadRequest("Request is not valid");
                    }
                    this.Logger.Debug("Request is valided...");
                    var files = context.Request.Form.Files.ToList();
                    dynamic documentData = context.Request.Form.FirstOrDefault().Value;

                    this.Logger.Debug($"Processing Files : {files.Count}");

                    var documentMetadata = Newtonsoft.Json.Linq.JObject.Parse(documentData);
                    var partnerAccountId = string.Empty;

                    if (documentMetadata != null)
                    {
                        if (documentMetadata.partnerAccountId != null)
                        {
                            partnerAccountId = documentMetadata.partnerAccountId;
                        }
                    }

                    string tenantIdNew = ParseTenantIdFromPartnerId(partnerAccountId);

                    if(!string.IsNullOrEmpty(tenantIdNew))
                        tenantId = tenantIdNew;

                    await this.FileThisDeliveryListener.Process(tenantId, documentData, files);
                    this.Logger.Debug("...Completed Document");
                    return Ok();
                }
                catch (NullReferenceException ex)
                {
                    this.Logger.Error($"Error in filethis-delivery: {ex.Message}");
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (Exception ex)
                {
                    this.Logger.Error($"Error in filethis-delivery: {ex.Message}");
                    return ErrorResult.InternalServerError(ex.Message);
                }
            });
        }

        private string ParseTenantIdFromPartnerId(string partnerAccountId)
        {
            if(string.IsNullOrWhiteSpace(partnerAccountId))
                return string.Empty;

            try
            {
                string[] list = partnerAccountId.Split(':');
                if(list.Length > 2)
                {
                    return list[0];
                }
                else
                    return string.Empty;
            }
            catch(Exception ex)
            {
                Logger.Error("Error while parsing PartnerAccountId for Tenant", ex);
                return  string.Empty;
            }
        }

        /// <summary>
        /// Validate Request body signature
        /// </summary>
        /// <param name="context"></param>
        /// <returns>bool</returns>
        private async Task<bool> ValidateRequest(HttpContext context)
        {
            var stream = context.Request.Body;
            var bytes = new byte[] { };
            Logger.Debug("Verify stream");
            if (stream.CanRead)
            {
                using (var ms = new MemoryStream())
                {
                    await stream.CopyToAsync(ms);
                    bytes = ms.ToArray();
                    ms.Position = 0L;
                    context.Request.Body = new MemoryStream(ms.ToArray());
                }
                this.Logger.Debug($"stream is verified");
            }

            this.Logger.Debug($"Validate Signature"); 
            if (!FilethisSignatureValidator.ValdiateSignature(context.Request.Headers, bytes))
            {
               throw new NullReferenceException("Signature is not valid");
            }
            return true;
        }
    }
}
