﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Docitt.FileThis.Delivery
{
    public static class FileThisHelper
    {
        internal static string GetHmacSignature(byte[] message, string secret)
        {
            var encoding = new ASCIIEncoding();
            var secretBytes = encoding.GetBytes(secret);
            using (var hmac = new HMACSHA256(secretBytes))
            {
                var signature = hmac.ComputeHash(message);
                return Convert.ToBase64String(signature);
            }
        }
    }
}
