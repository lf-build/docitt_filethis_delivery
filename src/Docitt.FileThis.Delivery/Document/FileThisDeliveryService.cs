﻿using Docitt.FileThis.Delivery.Abstraction;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace Docitt.FileThis.Delivery
{
    public class FileThisDeliveryService : IFileThisDeliveryService
    {
        public FileThisDeliveryService(
            IFileThisDeliveryLogRepository repository,
            IDocumentManagerService documentManagerClient,
            IEventHubClient eventHub,
            IConfiguration configuration,
            ILogger logger)
        {
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));

            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            this.Configuration = configuration;
            this.DocumentManagerService = documentManagerClient;
            this.EventHub = eventHub;
            this.Logger = logger;
            this.Repository = repository;
        }

        private IFileThisDeliveryLogRepository Repository { get; }

        private IEventHubClient EventHub { get; }

        private IDocumentManagerService DocumentManagerService { get; }

        private Abstraction.IConfiguration Configuration { get; }

        private ILogger Logger { get; }

        public async Task<bool> GetFileThisDocument(dynamic documentData, List<IFormFile> files)
        {
            this.Logger.Info("Started GetFileThisDocument...");
            IDocumentMetaData metaData = ExtractMetaData(documentData);
            metaData.Name = "document_delivery_notification";

            this.Logger.Info($"{JsonConvert.SerializeObject(metaData, Formatting.Indented)}");
            this.Logger.Info("Insert into log table...");
            metaData.TenantId = this.ParseTenantIdFromPartnerId(metaData.PartnerCustomerId);
            Logger.Debug("TenantID :" + metaData.TenantId);
            await this.Repository.AddDocumentMetaData(metaData);

            this.Logger.Info("Loop through IFormFile...");
            foreach (var file in files)
            {
                if (file != null && file.Length > 0)
                {
                    this.Logger.Info($"DocumentManagerService.Create(entityid={metaData.CustomerId},filename={metaData.Filename}");

                    var tags = new List<string>() { metaData.Institution };

                    this.Logger.Info("Create now");
                    this.Logger.Info("EntityType used in Document Manager: " + Delivery.Abstraction.Settings.DocumentManagerEntityType);
                    this.Logger.Info("PartnerCustomerId : " + metaData.PartnerCustomerId);
                    var result = await DocumentManagerService.Create(
                        file.OpenReadStream(),
                        Delivery.Abstraction.Settings.DocumentManagerEntityType,
                        metaData.CustomerId,
                        metaData.Filename,
                        JsonConvert.SerializeObject(metaData, Formatting.Indented),
                        tags
                        );

                    this.Logger.Info($"Raise Event : {nameof(DocumentMetaData)}");
                    metaData.DocittDocumentId = result.Id;

                    EventHub.Publish(nameof(DocumentMetaData), metaData).Wait();
                }
            }

            this.Logger.Info($"...Complete GetFileThisDocument");
            return true;
        }

        private string ParseApplicantIdFromPartnerId(string partnerAccountId)
        {
            if(string.IsNullOrWhiteSpace(partnerAccountId))
                return string.Empty;

            try
            {
                Logger.Debug("Parsing :" + partnerAccountId);
                string[] list = partnerAccountId.Split(':');
                return list[list.Length-1];
            }
            catch(Exception ex)
            {
                Logger.Error("Error while parsing PartnerAccountId", ex);
                return partnerAccountId;
            }
        }

        private string ParseTenantIdFromPartnerId(string partnerAccountId)
        {
            if(string.IsNullOrWhiteSpace(partnerAccountId))
                return string.Empty;

            try
            {
                string[] list = partnerAccountId.Split(':');
                if(list.Length > 2)
                {
                    return list[0];
                }
                else
                    return string.Empty;
            }
            catch(Exception ex)
            {
                Logger.Error("Error while parsing PartnerAccountId for Tenant", ex);
                return  string.Empty;
            }
        }

        private IDocumentMetaData ExtractMetaData(dynamic documentData)
        {
            this.Logger.Info("Started ExtractMetaData...");

            IDocumentMetaData document = new DocumentMetaData();
            var documentMetadata = Newtonsoft.Json.Linq.JObject.Parse(documentData);

            if (documentMetadata != null)
            {
                if (documentMetadata.partnerAccountId != null)
                {
                    document.PartnerCustomerId = documentMetadata.partnerAccountId;
                    Logger.Debug("Parsing ParseApplicantIdFromPartnerId");
                    document.CustomerId = this.ParseApplicantIdFromPartnerId(document.PartnerCustomerId);
                }
                    

                if (documentMetadata.document != null)
                {
                    var documents = documentMetadata.document;

                    if (documents.connectionId != null) document.ConnectionId = documents.connectionId;
                    if (documents.documentId != null) document.DocumentId = documents.documentId;
                    if (documents.date != null) document.Date = documents.date.ToString();
                    if (documents.sourceId != null) document.SourceId = documents.sourceId.ToString();
                    if (documents.institution != null) document.Institution = documents.institution;
                    if (documents.filename != null) document.Filename = documents.filename;
                    if (documents.documentType != null) document.DocumentType = documents.documentType;
                    if (documents.documentSubtype != null) document.DocumentSubType = documents.documentSubtype;
                    if (documents.actionDate != null) document.ActionDate = documents.actionDate?.ToString();
                    if (documents.accounts != null)
                    {
                        var accounts = documents.accounts;
                        var account = accounts[0];

                        if (account.accountName != null) document.AccountName = account.accountName;
                        if (account.accountNumber != null) document.AccountNumber = account.accountNumber;
                        if (account.accountType != null) document.AccountType = account.accountType;
                        if (account.accountSubtype != null) document.AccountSubtype = account.accountSubtype;
                        if (account.totalAmountDue != null) document.TotalAmountDue = account.totalAmountDue?.ToString();
                        if (account.lastPaymentAmount != null) document.LastPaymentAmount = account.lastPaymentAmount?.ToString();
                        if (account.minimumAmountDue != null) document.MinimumAmountDue = account.minimumAmountDue?.ToString();
                        if (account.dateDue != null) document.DueDate = account.dateDue?.ToString();
                        if (account.currentBalance != null) document.CurrentBalance = account.currentBalance?.ToString();
                    }

                    if (documents.period != null)
                    {
                        if (documents.period.startDate != null) document.StartDate = documents.period?.startDate?.ToString();
                        if (documents.period.endDate != null) document.EndDate = documents.period?.endDate?.ToString();
                    }
                }
            }

            return document;
        }
    }


}