﻿using Docitt.FileThis.Delivery.Abstraction;
using Docitt.FileThis.Delivery.Persistence;
using LendFoundry.Configuration;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace Docitt.FileThis.Delivery
{
    public class FileThisDeliveryServiceFactory : IFileThisDeliveryServiceFactory
    {
        public FileThisDeliveryServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFileThisDeliveryService Create(ITokenReader reader,ILogger logger, string tenantName)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Abstraction.Configuration>(
                                                    Delivery.Abstraction.Settings.ConfigurationServiceName,
                                                    reader);
            var configuration = configurationService.Get();

            //Document Manager
            var documentFactory = Provider.GetService<IDocumentManagerServiceFactory>();
            var documentManagerClient = documentFactory.Create(reader);
            
            //Event Hub
            var evenHubClientFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubClient = evenHubClientFactory.Create(reader);

            //Repository
            var repositoryFactory = Provider.GetService<IRepositoryFactory>();
            var deliveryLogRepository = repositoryFactory.CreateFileThisDeliveryRepository(reader, configuration);

            return new FileThisDeliveryService(deliveryLogRepository, documentManagerClient, eventHubClient, configuration, logger);
        }
    }
}
