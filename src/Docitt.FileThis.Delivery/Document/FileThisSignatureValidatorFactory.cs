﻿using Docitt.FileThis.Delivery.Abstraction;
using Docitt.FileThis.Delivery.Persistence;
using LendFoundry.Configuration;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace Docitt.FileThis.Delivery
{
    public class FileThisSignatureValidatorFactory : IFileThisSignatureValidatorFactory
    {
        public FileThisSignatureValidatorFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFileThisSignatureValidator Create(ITokenReader reader,ILogger logger)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<Abstraction.Configuration>(
                                                    Delivery.Abstraction.Settings.ConfigurationServiceName,
                                                    reader);
            var configuration = configurationService.Get();

            return new FileThisSignatureValidator(configuration, logger);
        }
    }
}
