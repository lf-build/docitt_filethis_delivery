﻿using Docitt.FileThis.Delivery.Abstraction;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;

namespace Docitt.FileThis.Delivery
{
    public class FileThisDeliveryListener : IFileThisDeliveryListener
    {
        public FileThisDeliveryListener(
            IFileThisDeliveryServiceFactory fileThisDeliveryServiceFactory,
            ITokenHandler tokenHandler,
            ILogger logger)
        {
            if (fileThisDeliveryServiceFactory == null)
                throw new ArgumentNullException(nameof(fileThisDeliveryServiceFactory));

            if (tokenHandler == null)
                throw new ArgumentNullException(nameof(TokenHandler));

            this.TokenHandler = tokenHandler;
            this.Logger = logger;
            this.FileThisDeliveryServiceFactory = fileThisDeliveryServiceFactory;
        }

        private ITokenHandler TokenHandler { get; }

        private IFileThisDeliveryServiceFactory FileThisDeliveryServiceFactory { get; }

        private ILogger Logger { get; }

        public async Task<bool> Process(string tenantName, dynamic documentData, List<IFormFile> files)
        {
            this.Logger.Debug("Started FileThisDeliveryListener.Process...");
            var tenantToken = TokenHandler.Issue(tenantName, Settings.ServiceName, null, "system", null);
            var reader = new StaticTokenReader(tenantToken.Value);
            this.Logger.Debug("Creating FileThisDeliveryServiceFactory...");
            var service = this.FileThisDeliveryServiceFactory.Create(reader, this.Logger, tenantName);

            this.Logger.Debug("Calling GetFileThisDocument...");
            var result = await service.GetFileThisDocument(documentData, files);

            this.Logger.Debug("...Completed FileThisDeliveryListener.Process");
            return result;
        }
    }
}
