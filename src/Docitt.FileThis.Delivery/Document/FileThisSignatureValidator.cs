﻿using Docitt.FileThis.Delivery.Abstraction;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Docitt.FileThis.Delivery
{
    public class FileThisSignatureValidator : IFileThisSignatureValidator
    {
        private int DelivierDelaySeconds { get; }

        private int HalfSkewMilliSeconds { get; }

        private Dictionary<string, DateTime> UsedNonce { get; }

        public FileThisSignatureValidator(IConfiguration configuration, ILogger logger)
        {
            this.DelivierDelaySeconds = 900;
            this.HalfSkewMilliSeconds = 15 / 2 * 1000;
            this.UsedNonce = new Dictionary<string, DateTime>();
            this.Configuration = configuration;
            this.Logger = logger;
        }

        private IConfiguration Configuration {get;}

        private ILogger Logger {get;}

        /// <summary>
        /// Validate Signature 
        /// </summary>
        /// <param name="headers">Headers</param>
        /// <param name="body">byte [] body</param>
        /// <param name="secret"></param>
        /// <returns></returns>
        public bool ValdiateSignature(IHeaderDictionary headers, byte[] body)
        {
            string secret = Configuration.Secret;
            var signatureHeader = headers.SingleOrDefault(header => header.Key == "X-FileThis-Signature");
            if (string.IsNullOrEmpty(signatureHeader.Key))
            {
               throw new NullReferenceException("X-FileThis-Signature header not found");
            }
            var signature = FileThisHelper.GetHmacSignature(body, secret);
            if (signature != signatureHeader.Value)
            {
                this.Logger.Error($"Signature is not valid and not maching {Configuration.Secret.Substring(0,4)}xxx ");
                return false;
            }
            return true;
        }
    }
}
