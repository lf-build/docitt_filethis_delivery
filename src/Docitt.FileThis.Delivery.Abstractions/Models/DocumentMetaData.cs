﻿using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.FileThis.Delivery.Abstraction
{
    public class DocumentMetaData : Aggregate, IDocumentMetaData
    {
        public string DocittDocumentId { get; set; }
        public string Name { get; set; } = "document_delivery_notification";
        public string MinimumAmountDue { get; set; }
        public string LastPaymentAmount { get; set; }
        public string SourceId { get; set; }
        public string Institution { get; set; }
        public string Filename { get; set; }
        public string EndDate { get; set; }
        public string DueDate { get; set; }
        public string DocumentType { get; set; }
        public string DocumentSubType { get; set; }
        public string DocumentId { get; set; }
        public string StartDate { get; set; }
        public string Date { get; set; }
        public string CurrentBalance { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ConnectionId { get; set; }
        public string ActionDate { get; set; }
        public string AccountType { get; set; }
        public string AccountSubtype { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string CustomerId { get; set; }
        public string PartnerCustomerId { get; set; }
        public string TotalAmountDue { get; set; }
    }
}
