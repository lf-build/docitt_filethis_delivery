﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace Docitt.FileThis.Delivery.Abstraction
{
    public interface IFileThisDeliveryLogRepository : IRepository<IDocumentMetaData>
    {
        Task<bool> AddDocumentMetaData(IDocumentMetaData metaData);
    }
}
