﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace Docitt.FileThis.Delivery.Abstraction
{
    public interface IConfiguration: IDependencyConfiguration
    {
        string Url { get; set; }
        string Version { get; set; }
        string Key { get; set; }
        string Secret { get; set; }
        string Ticket { get; set; }
        string CallbackDeliveryUrl { get; set; }
        int AccountTokenExpiration { get; set; }
        FileThisSignatureValidatorConfig FileThisSignatureValidator { get; set; }
        string ConnectionString { get; set; }
    }

    public class Configuration : IConfiguration
    {
        public string Url { get; set; }
        public string Version { get; set; }
        public string Key { get; set; }
        public string Secret { get; set; }
        public string Ticket { get; set; }
        public string CallbackDeliveryUrl { get; set; }
        public int AccountTokenExpiration { get; set; }

        public FileThisSignatureValidatorConfig FileThisSignatureValidator { get; set; }

        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }

    public class FileThisSignatureValidatorConfig
    {
        public int ClockSkewSeconds { get; set; }
        public int DeliveryDelaySeconds { get; set; }
    }
}

