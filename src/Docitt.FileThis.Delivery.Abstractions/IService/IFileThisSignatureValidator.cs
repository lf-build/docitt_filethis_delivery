﻿#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif

namespace Docitt.FileThis.Delivery.Abstraction
{
    public interface IFileThisSignatureValidator
    {
        bool ValdiateSignature(IHeaderDictionary headers, byte[] body);
    }
}
