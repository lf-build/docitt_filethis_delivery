﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.FileThis.Delivery.Abstraction
{
    public interface IFileThisSignatureValidatorFactory
    {
        IFileThisSignatureValidator Create(ITokenReader reader, ILogger logger);
    }
}
