﻿#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.FileThis.Delivery.Abstraction
{
    public interface IFileThisDeliveryListener
    {
        Task<bool> Process(string tenantName, dynamic documentData, List<IFormFile> files);
    }
}
