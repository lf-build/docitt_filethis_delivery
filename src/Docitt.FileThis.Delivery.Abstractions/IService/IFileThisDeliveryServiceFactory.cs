﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace Docitt.FileThis.Delivery.Abstraction
{
    public interface IFileThisDeliveryServiceFactory
    {
        IFileThisDeliveryService Create(ITokenReader reader, ILogger logger, string tenantName);
    }
}
